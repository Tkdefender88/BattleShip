module gitea.justinbak.com/juicetin/bsStatePersist/battleGo

go 1.13

require (
	github.com/alexandrevicenzi/go-sse v1.5.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/gorilla/sessions v1.2.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
)
